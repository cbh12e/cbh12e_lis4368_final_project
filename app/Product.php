<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App
 */
class Product extends Model {

    /**
     * The connection that is used by the model.
     * @var string $connection
     */
    protected $connection ='mysql2';

    /**
     * This gets the table information
     * @var string $table
     */
    protected $table = 'product';

    /**
     *This gets the fillable fields to insert into the database
     * @var array $fillable
     */
    protected $fillable =['id', 'pro_name','pro_type','pro_size','pro_resolution','pro_price','pro_description', 'pro_photo'];

}