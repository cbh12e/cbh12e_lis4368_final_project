<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Validation\Validator;

class ProductStoreRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'brand'      => 'required|max:45',
            'model'      => 'required|max:15',
            'size'       => 'required|integer',
            'resolution' => 'required',
            'type'       => 'required',
            'price'      => 'required|integer',
            'image'      => 'image',
            'notes'     => 'max:255'
		];
	}

}
