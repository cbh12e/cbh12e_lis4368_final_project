<?php namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Image;
use App\Product;

use Illuminate\Support\Facades\Input;
use Illuminate\Session;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductController
 *
 * @author cbh12e
 *
 *
 */
class ProductController extends Controller{

    /**
     * @param \App\Product $product
     */
    protected $product;

    /**
     * @param \App\Product $product

    public function __construct(ProductStoreRequest $product)
    {
        $this->product = $product;
    }

     */
    /**
     * @var
     */
    protected $welcome;

    /**
     * This function selects all the statements to prepare
     * for execution to show all of the products.
     *
     * @return $this
     */
    public function selectAll()
{
    /**
     * This gets all the products for the product table
     * @var = $welcome
     */

    /** @var Product $welcome
     * This returns all products and
     * it returns five columns at a
     * time to reduce clutter/
     */
    $welcome = Product::paginate(5);

    /**
     * Redirect the visitor to the page showing all products in the products table
     *
     */
   return view('welcome')->with('welcome', $welcome);
}

    /**
     * @param $id
     *
     * @return mixed
     */

    /**
     * @return \Illuminate\View\View
     *
     * Redirects the user to the page
     * to add a product.
     */
    public function addProduct()
    {
        return view('form');
    }

    /**
     * @param $product
     */


    public function insertProduct(ProductStoreRequest $product)
    {
        $product = new Product;
        /** @var array $product */
        $product->pro_name = Input::get('brand');
        /** @var array $product->brand */
        $product->pro_model = Input::get('model');
        /** @var string $product->model */
        $product->pro_size = Input::get('size');
        /** @var array $product->resolution */
        $product->pro_resolution = Input::get('resolution');
        /** @var array $product->type */
        $product->pro_type = Input::get('type');
        /** @var array $product->price */
        $product->pro_price = Input::get('price');
        /** @var array $product->description */
        $product->pro_description = Input::get('notes');

        /*
         * Uploading image to destination. If it passes,
         * it will be passed on to enter the link into the
         * database so that the image can be retrieved later.
         */
        if (Input::file('image')->isValid())
        {
            /** @var array $destinationPath
             *  @var array $extension
             *  @var array $fileName
             *  @var array $file1
             */
            $destinationPath = 'uploads'; // upload path
            $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
            $file1 = ''.$destinationPath.'/'.$fileName.'';

        /** @var array $product */
        $product->pro_photo = $file1;
        }

        /** @var Product $product
         * @return @url */
        return $product->save() ? redirect('success') : "Database Error. Record cannot be added at this time.";

    }
}
