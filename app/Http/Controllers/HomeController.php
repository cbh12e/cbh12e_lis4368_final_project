<?php namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\View;

/**
 * Class HomeController
 *
 * @package App\Http\Controllers
 *
 */

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
        /** @return $this */
        $this->middleware('auth');
	}

    /**
     * This sends the user to the form to add a new product
     * @return \Illuminate\View\View
     */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('form');
	}

    /**
     * This sends the user to the about page to the user about the fake business.
     * @return \Illuminate\View\View
     */
    public function about()
    {

        return view('about');
    }
    /**
     * This sends the user to the users page to show all the users signed up.
     * @return \Illuminate\View\View
     */
    public function users()
    {

        /**
         * @var array $users
         * @return void
         */
        $users =  User::all();
        return view('users')->with('users',$users);
    }
}
