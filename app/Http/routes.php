<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ProductController@selectAll');
Route::get('success', 'ProductController@selectAll');
Route::get('/success', 'ProductController@selectAll');
Route::post ('/add', 'ProductController@insertProduct');
Route::get('/add', ['middleware' => 'auth', 'uses' => 'ProductController@addProduct']);
Route::get('home', 'HomeController@index');
Route::get('info', 'WelcomeController@info');
Route::get('about', 'HomeController@about');
Route::get('users', 'HomeController@users');

/*
 * Use this for Laravel 5
 */


Route::get('single/{id}', /**
 * @param $id
 *
 * @return $this
 */
    function($id)
{

    $single = \App\Product::find($id);

    //return (var_dump($single));
   return view('single')->with('single',$single);

});

Route::get('details/{id}', /**
 * @param $id
 *
 * @return $this
 */
    function($id)
    {

        $details = \App\Product::find($id);

        //return (var_dump($single));
        return view('details')->with('details',$details);

    });

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);




