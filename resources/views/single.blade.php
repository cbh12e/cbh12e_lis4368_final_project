@extends('app')
@section('title')
Featured
@endsection
@section('content')
        <h1 class="text-center">Rocket Television's Best Televisions currently in stock!</h1>
        <a href="{{url('/')}}" class="btn btn-primary" role="button"><i class="fa fa-arrow-left"></i> Back</a>
        <table class="table table-striped">
            <thead>
            <tr>
                <td> Product ID</td>
                <td> Maker</td>
                <td> Model</td>
                <td> Size </td>
                <td> Resolution </td>
                <td> Type </td>
                <td> Price</td>
                <td> Description</td>
                <td></td>
            </tr>
            <tbody>
                <tr>
                    <td>{{$single->id }}</td>
                    <td>{{$single->pro_name }}</td>
                    <td>{{$single->pro_type }}</td>
                    <td>{{$single->pro_size }}</td>
                    <td>{{$single->pro_resolution }}</td>
                    <td>{{$single->pro_type }}</td>
                    <td>${{$single->pro_price }}</td>
                    <td>{{$single->pro_description }}</td>
                    <td><a href="{{url('details/'.$single->id)}}" class="btn btn-info"><i class="fa fa-info"></i> Details</a> </td>
                </tr>
            </tbody>
        </table>
@endsection