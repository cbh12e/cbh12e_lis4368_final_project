@extends('app')
@section('title')
Users
@endsection
@section('content')
    <section class="content-header">
        <h1><strong>
                <i class="fa fa-user"></i> User Profile</strong>
        </h1>
    </section>
    <hr>
    <table class="table table-striped">
         <thead>
         <tr>
             <td> Name</td>
             <td> Email</td>
             <td> Created</td>
             <td> Last Signed in</td>
         </tr>
         <tbody>
         @foreach($users as $row)
             <tr>
             <td>{{$row->name}}</td>
             <td>{{$row->email }}</td>
             <td>{{date('F d Y',strtotime( $row->created_at))}}</td>
             <td>{{date('F d Y',strtotime( $row->updated_at))}}</td>
     </tr>
        @endforeach
 </table>
@endsection