<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Rocket Televisions | @yield('title')</title>
    <link type="text/css" rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" >
    <link type="text/css" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link type="image/ico" rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    @yield('css')

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
                <a class="navbar-brand" href="#"><img src="{{asset('images/logo_main_small.png')}}" alt="Logo" style="margin-top:-12px"> Rocket Televisions</a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
                    @if (Auth::guest())
					<li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
					<li><a href="{{ url('info') }}"><i class="fa fa-info"></i> About Us</a></li>
                    @else
                    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                        <li><a href="{{ url('about') }}"><i class="fa fa-info"></i> About Us</a></li>
                    <li><a href="{{ url('/add') }}"><i class="fa fa-desktop"></i> Add New Product</a></li>
                    @endif
				</ul>
				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-user"></i>  {{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('users') }}"><i class="fa fa-users"></i> User Management</a></li>
								<li><a href="{{ url('/auth/logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>
<div class="container">
	@yield('content')
</div>
    <hr>
    <div id="footer">
        <p class="muted credit text-center">&#169; {{date("Y")}}
            Rocket Televisions LLC. &bull;
            115 York Avenue &bull;
            Monticello, FL 32344 &bull; (850) 555-1234
        </p>
    </div>
	<!-- Scripts -->
	<script src="{{ asset('js/jquery.min.js')}}"></script>
	<script src="{{ asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
