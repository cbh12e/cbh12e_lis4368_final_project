@extends('app')
@section('title')
Product Info
@endsection
@section('content')
    <section class="content-header">
        <h1 class="text-center" style="font-size:55px;"><strong>
                <i class="fa fa-desktop"></i> Product Details</strong>
        </h1>
    </section>
    <hr>

@if(($details->pro_photo) == true)
    <img src="{{url($details->pro_photo)}}" alt="Product Image" class="img-thumbnail img-responsive center-block" height="300" width="300">
    @endif


        <form class="form-horizontal">
            <div class="col-md-3 col-md-offset-3">
            <div class="row">


            <div class="form-group">
                <label class="col-md-3 control-label">Product ID:</label>
                <div class="col-md-3">
                    <p class="form-control-static">{{$details->id }}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">Product Brand:</label>
                <div class="col-md-3">
                    <p class="form-control-static">{{$details->pro_name }}</p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Product Model:</label>
                <div class="col-md-3">
                    <p class="form-control-static">{{$details->pro_model }}</p>
                </div>
            </div>
                <div class="form-group">
                <label class="col-md-3 control-label">Product Type:</label>
                <div class="col-md-3">
                    <p class="form-control-static">{{$details->pro_type }}</p>
                </div>
                </div>
            </div>
                </div>


<div class="row">
            <div class="col-md-6">
                <div class="form-group">
                <label class="col-md-2 col-md offset-1 control-label">Product Size:</label>
                <div class="col-md-3 ">
                    <p class="form-control-static">{{$details->pro_size }}</p>
                </div>
            </div>
                <div class="form-group">
                <label class="col-md-2 col-md offset-1 control-label">Product Resolution:</label>
                <div class="col-md-3 ">
                    <p class="form-control-static">{{$details->pro_resolution }}</p>
                </div>
            </div>
                <div class="form-group">
                <label class="col-md-2 col-md offset-1 control-label">Product Type:</label>
                <div class="col-md-3 ">
                    <p class="form-control-static">{{$details->pro_type }}</p>
                </div>
            </div>
                <div class="form-group">
                <label class="col-md-2 col-md offset-1 control-label">Product Price:</label>
                <div class="col-md-3">
                    <p class="form-control-static">${{$details->pro_price }}</p>
                </div>
            </div>


            </div>
            </div>

    <div class="form-group">
                <label class="col-md-3 col-md offset-5 control-label">Product Description:</label>
                <div class="col-md-5">
                    <p class="form-control-static">{{$details->pro_description }}</p>
                </div>
            </div>


        </form>

@endsection