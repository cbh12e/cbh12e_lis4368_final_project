@extends('app')
@section('title')
Home
@endsection
@section('content')
    <div class="container">

        <h1 class="text-center">Rocket Television's Best Televisions currently in stock!</h1>
        <a href="{{ url('single/8')}}" class="btn btn-primary" role="button"><i class="fa fa-desktop"></i>  Our Featured Product</a>
        <table class="table table-striped" id="tv">
            <thead>
            <tr>
                <td> Product ID</td>
                <td> Maker</td>
                <td> Size </td>
                <td> Resolution </td>
                <td> Price</td>
                <td></td>
            </tr>

            <tbody>

            @foreach($welcome as $row)

                <tr>
                    <td>{{$row->id }}</td>
                    <td>{{$row->pro_name }}</td>
                    <td>{{$row->pro_size }}</td>
                    <td>{{$row->pro_resolution }}</td>
                    <td>${{$row->pro_price }}</td>
                    <td><a href="{{ asset('details/'.$row->id) }}" target="_blank" class="btn btn-info"><i class="fa fa-info"></i>  Details</a> </td>
                </tr>
            @endforeach
            </tbody>


        </table>
        <div class="text-center">
            <?php echo $welcome->render(); ?>
        </div>

    </div>
@endsection