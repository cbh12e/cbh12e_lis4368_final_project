@extends('app')
@section('title')
About Us
@endsection
@section('css')
    <link href="{{ asset('css/map.css') }}" rel="stylesheet">
    <link href="{{ asset('css/aboutus.css') }}" rel="stylesheet">
@endsection
@section('content')
    <section class="content-header">
        <h1 class="text-center">
            <img src="{{url('images/logov.png')}}" alt="logo" class="img-thumbnail"/> About Rocket Televisions
        </h1>
    </section>
    <hr>
    <!-- Google Maps Location iFrame-->
    <div class="col-sm-6 col-md-5 col-md-offset-2 col-lg-6 col-lg-offset-0">
        <p style="font-size:17px;">
            Since the founding of Rocket Televisions in 2015, Rocket Televisions is a family owned and operated business that specializes in selling televisions to customers nationwide. Rocket Televisions only sells top of the line
            televisions that will satisfy you or we will refund you if you are not satisified with our products within a thirty day period. Rocket Television's roots come from Monticello, a rural town in the Florida Panhandle located 30 miles east of the state capital: Tallahassee, Florida.

            Since Rocket Televisions specializes in television sets, we sell the major brands of televisions which includes Cosmo, Samsung, Panasonic, LG, Sony, Sharp, and more. What is better about us is that we unlike the big box retailers is that we sell our televisions for less. The best about us is that we will ship our television sets right to your door anywhere in the United States from our brand new efficient 100,000 square foot warehouse powered by 100% of renewable energy from <strong>Lake Talquin</strong>.</p>

    </div>
    <div class="col-sm-6 col-md-5 col-lg-6">
        <div class="text-center">
            <iframe class="img-responsive" id="map" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d220014.978363252!2d-84.01447006328124!3d30.50027651293717!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1425184649329"></iframe>
        </div>
    </div>
@endsection