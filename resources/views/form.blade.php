<?php
/**
 * Created by PhpStorm.
 * User: hargcb
 * Date: 4/18/2015
 * Time: 1:05 PM
 */
use Illuminate\Validation;
        ?>
@extends('app')
@section('title')
Add Product
@endsection
@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <section class="content-header">
        <h1 class="text-center">
                <i class="fa fa-desktop"></i> Add New Television
        </h1>
    </section>

    <hr>
    <form method="POST" action="{{url('/add')}}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

<fieldset>

    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <!-- Brand Maker-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="brand">Brand</label>
        <div class="col-md-3">
            <input type="text" name="brand" id="brand" class="form-control"  value="{{ old('brand') }}" placeholder="Brand of Television" />
        </div>
    </div>

    <!-- TV Model-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="model">Model Number: *</label>
        <div class="col-md-3">
            <input type="text" name="model" class="form-control" id="model" placeholder="Model Number"  value="{{ old('model') }}"/>
        </div>
    </div>

    <!-- TV Size-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="size">Size of Television *</label>
        <div class="col-md-1">
            <input type="text" name="size" id="size"  value="{{ old('size') }}" class="form-control"  placeholder="32" />
        </div>
    </div>

    <?php
    /* Attn: Resolution Type Radio Options
     * uses form styles options from Laravel 4
     * only for radios to print out values if
     * validation fails. Others were used
     * through newer Laravel 5 style. This
     * was discussed during help session during
     * instructor evaluations.
     */ ?>

    <!-- TV Resolution -->
    <div class="form-group">
        <label class="col-md-4 control-label">Resolution:</label>
        <label class="radio-inline">
            {!!Form::radio('resolution', '480p')!!}  480p
        </label>
        <label class="radio-inline">
            {!!Form::radio('resolution', '720p')!!} 720p
        </label>
        <label class="radio-inline">
            {!!Form::radio('resolution', '1080i')!!} 1080i
        </label>
        <label class="radio-inline">
            {!!Form::radio('resolution', '1080p')!!} 1080p
        </label>
    </div>

    <?php
   /* TV Type Radio Options
    * uses form styles options from Laravel 4
    * only for radios to print out values if
    * validation fails. Others were used
    * through newer Laravel 5 style. This
    * was discussed during help session during
    * instructor evaluations
    */ ?>

    <!-- Television Type-->
    <div class="form-group">
        <label class="col-md-4 control-label">Type:</label>
        <label class="radio-inline">
            {!!Form::radio('type', 'CRT')!!} CRT
        </label>
        <label class="radio-inline">
            {!!Form::radio('type', 'LED')!!} LED
        </label>
        <label class="radio-inline">
            {!!Form::radio('type', 'Plasma')!!} Plasma
        </label>
        <label class="radio-inline">
            {!!Form::radio('type', '3D')!!} 3D
        </label>
    </div>

    <!-- Product Price-->
    <div class="form-group">
        <label class="col-md-4 control-label" for="price">Price *</label>
        <div class="col-md-2">
            <input type="text" name="price" id="price"  value="{{ old('price') }}" class="form-control"  placeholder="100" />
        </div>
    </div>

    <!-- Product Notes -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="notes">Product Description:</label>
        <div class="col-md-4">
            <textarea name="notes" id="notes" class="form-control"></textarea>
        </div>
    </div>

    <!-- Add Image to Product-->
    <div class="form-group">
        <label class="col-md-4 control-label">Product Image:</label>
        <div class="col-md-4">
            <input type="file" name="image"/>
        </div>
    </div>

    <!-- Submit Button -->
    <div class="form-group">
        <label class="col-md-4 control-label" for="submit"></label>
        <div class="col-md-4">
            <button type="submit" name="submit" id="submit" class="btn btn-primary btn-lg"><i class="fa fa-check"></i> Submit</button>
            <button type="reset" class="btn btn-default btn-lg" value="Reset" title="Reset to type the information again">	<i class="fa fa-times"></i> Reset </button>
        </div>
    </div>
</fieldset>
</form>
@endsection